#!/usr/bin/env python3
"""
ToeTally:

A simple script that draws cards in a
framebuffer or X window.
"""

import sys
import os

# Make pygame import silent
from contextlib import redirect_stdout
with redirect_stdout(None):
    import pygame

pygame.init()
pygame.font.init()
pygame.display.set_caption('ToeTally')

# Some initial variables
# TODO: Get from config file
SPEED = [0, 0]
TITLE_FONT = "Cantarell Extra Bold"
BODY_FONT = "Cantarell Bold"
# Set some colours
BLACK = 0, 0, 0
DARK_GREY = 40, 40, 40
GREY = 100, 100, 100
LIGHT_GREY = 200, 200, 200
RED = 180, 38, 34
GREEN = 37, 110, 51
CYAN = 40, 200, 200
PURP = 70, 10, 140
BLUE = 80, 80, 250
WHITE = 255, 255, 255


def display():
    """Set display options"""
    global screen, SCREEN_WIDTH, SCREEN_HEIGHT # pylint:disable=invalid-name
    if 'DISPLAY' in os.environ and os.environ['DISPLAY'] == "":
        # Run full-screen if we're not under X
        SCREEN_WIDTH = 0
        SCREEN_HEIGHT = 0
    else:
        # Run in a window under X
        SCREEN_WIDTH = 800
        SCREEN_HEIGHT = 480

    size = SCREEN_WIDTH, SCREEN_HEIGHT

    if 'DISPLAY' in os.environ and os.environ['DISPLAY'] == "":
        screen = pygame.display.set_mode((size), pygame.FULLSCREEN)
        pygame.mouse.set_visible(False)
    else:
        screen = pygame.display.set_mode((size), pygame.NOFRAME)

    SCREEN_WIDTH, SCREEN_HEIGHT = pygame.display.get_surface().get_size()


def card_line(text, color, font, height):
    """Write a line on a card"""
    label = font.render(text, 1, color)
    text_rect = label.get_rect(center=(SCREEN_WIDTH/2, height))
    screen.blit(label, text_rect)


def drawcard(title, titlebgcolour, bgcolour,
             line_1, line_2, line_3, line_4, line_5):
    """Draws a card with a title and 5 lines of text"""
    screen.fill(bgcolour)
    pygame.draw.rect(screen, (titlebgcolour), (50, 50, SCREEN_WIDTH-110, 110))
    titlefont = pygame.font.SysFont(TITLE_FONT, 120)
    label = titlefont.render(title, 1, WHITE)
    text_rect = label.get_rect(center=(SCREEN_WIDTH/2, 105))
    screen.blit(label, text_rect)

    textfont = pygame.font.SysFont(BODY_FONT, 50)

    card_line(line_1, LIGHT_GREY, textfont, 220)
    card_line(line_2, LIGHT_GREY, textfont, 270)
    card_line(line_3, LIGHT_GREY, textfont, 320)
    card_line(line_4, LIGHT_GREY, textfont, 370)
    card_line(line_5, LIGHT_GREY, textfont, 420)

    pygame.display.flip()


def welcome():
    """Draw a welcome card"""
    drawcard("WELCOME", GREY, DARK_GREY,
             "",
             "Check README.md for keyboard shortcuts.",
             "",
             "Press a button (ex: 'f') to load a test card.",
             "")


def main():
    """Main function"""
    display()
    welcome()
    while True:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if (event.key == pygame.K_ESCAPE) or (event.key == pygame.K_q):
                    print("Escape pressed, exiting...")
                    sys.exit()
                if (event.key == pygame.K_l):
                    drawcard("LIVE", RED, DARK_GREY,
                             "This camera is on air.",
                             "",
                             "Please do not make any",
                             "sudden movements.",
                             "")
                if (event.key == pygame.K_j):
                    drawcard("LIVE", RED, DARK_GREY,
                             "MESSAGE FROM DIRECTOR.",
                             "",
                             "Zoom in more, please.",
                             "",
                             "")
                if (event.key == pygame.K_g):
                    drawcard("LIVE", RED, DARK_GREY,
                             "MESSAGE FROM DIRECTOR.",
                             "",
                             "Stop moving the camera!.",
                             "You are live!!!",
                             "")
                if (event.key == pygame.K_f):
                    drawcard("FREE", GREEN, DARK_GREY,
                             "This camera is not live.",
                             "",
                             "You may move it swiftly to",
                             "a new position.",
                             "")
                if (event.key == pygame.K_s):
                    drawcard("STAND BY", GREY, DARK_GREY,
                             "",
                             "No stream or recording is",
                             "currently taking place.",
                             "",
                             "")
                if (event.key == pygame.K_e):
                    drawcard("ERROR", GREY, RED,
                             "Connection to director lost.",
                             "",
                             "Attempting to reconnect...",
                             "Move camera with caution.",
                             "Camera may still be live.")
                if (event.key == pygame.K_m):
                    drawcard("MESSAGE", BLUE, DARK_GREY,
                             "Message from director:",
                             "",
                             "Please zoom out slightly, an",
                             "additional speaker is expected to join.",
                             "")
                if (event.key == pygame.K_a):
                    drawcard("SETUP", BLUE, DARK_GREY,
                             "",
                             "My IP is 10.1.14.78.",
                             "I am ready to be set up.",
                             "",
                             "")
                if (event.key == pygame.K_r):
                    drawcard("READY", PURP, DARK_GREY,
                             "",
                             "The video team is ready",
                             "to start the session.",
                             "",
                             "")
                if (event.key == pygame.K_o):
                    drawcard("OVER TIME!", RED, DARK_GREY,
                             "Dear talkmeister,",
                             "",
                             "This session is over time.",
                             "Broadcast will end abruptly.",
                             "")

            if event.type == pygame.QUIT:
                sys.exit()


if __name__ == "__main__":
    main()
